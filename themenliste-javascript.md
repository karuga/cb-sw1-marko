Vertiefungsthemen sind mit ** gekennzeichnet

# JavaScript

- Variablen definieren mit `let` und `const`
- primitive Datentypen
  - Überblick über Datentypen
  - Datentypen konvertieren
- Vergleiche mit == und ===
- for ... in - Schleifen
- Funktionen definieren
  - `function` Statement
  - Pfeilfunktionen
    - Weglassen von Klammern bei der Definition
- Arrays
  - Erstellen von Arrays
  - Elemente auslesen und setzen
  - Länge eines Arrays bestimmen
  - `.push()`, `.pop()`, ...
  - Funktionen höherer Ordnung, Callbacks: `.filter()`, `.sort()`, `.find()`, ...
- Objekte
  - Erstellen von Objekten
  - Properties auslesen und setzen
    - mit "."
    - mit "[]"
    - optional chaining
    - Definieren von Gettern und Settern
  - Object Property Shorthand **
  - Funktionen (Methoden) in Objekten definieren und auf `this` zugreifen **
- Arrays und Objekte: Zugreifen auf Einträge in verschachtelten Strukturen
- Fehler finden und beseitigen
  - Fehlermeldungen (Tracebacks) lesen
  - verwenden von `console.log()` und `console.trace()`
  - Debugger
    - Breakpoints

# JavaScript im Browser

- Scripts in HTML-Dokumenten einbinden
- HTML-Elemente finden
  - nach ID, Klassen, Tag-Namen
  - nach CSS-Selektoren
- Arbeiten mit Events
  - Eventlistener setzen
  - Arbeiten mit dem Event-Objekt
  - `.preventDefault()`
  - Event Bubbling
    - Event Bubbling aufhalten
  - Event Capturing
  - `target` und `currentTarget`
- Inhalte und Attribute von HTML-Elementen abfragen und ändern
  - Zugreifen auf Attribute von Elementen
    - Zugreifen auf dataset-Einträge
  - `innerText`, `innerHTML`
