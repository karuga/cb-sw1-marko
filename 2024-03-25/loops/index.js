////////////
// loops mit Arrays (und ähnlichem)
////////////

let a = ["foo", "bar", "baz"];

for (let i = 0; i < a.length; i++) {
    console.log(a[i]);
}

for (let index in a) {
    console.log(index);
    console.log(a[index]);
}

for (let item of a) {
    console.log(item);
}

let allButtons = document.querySelectorAll("button");

for (let button of allButtons) {
    button.addEventListener("click", () => {alert("hello 1");})
}

let allButtonsArray = Array.from(allButtons);

for (let i = 0; i < allButtonsArray.length; i ++) {
    allButtonsArray[i].addEventListener("click", () => {alert("hello 2")})
}

for (let index in allButtonsArray) {
    allButtonsArray[index].addEventListener("click", () => {alert("hello 3")})
}

////////////
// loops mit Objekten
////////////

let trainer = {
    firstName: "Marko",
    lastName: "Knöbl",
    birthYear: 1988
}

for (let key in trainer) {
    console.log(key);
    console.log(trainer[key])
}
