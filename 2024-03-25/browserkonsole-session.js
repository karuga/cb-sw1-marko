// Dies ist eine Kopie meiner JS-Session in der interaktiven Browserkonsole
// Sie enthält Ein- und Ausgaben

12 + 23
35
Boolean(0)
false
Boolean(1)
true
Boolean("")
false
Boolean(NaN)
false
Boolean(undefined)
false
Boolean(null)
false
Boolean("xyz")
true
let myString = "a";
if (myString) {
  console.log("xyz")
}
myString = "";
""
if (myString) {
  console.log("xyz")
}
!!""
false
Boolean("")
false
parseInt("123.45asde3ds3")
123
parseFloat("123.45abcd")
123.45
Number("123.45abc")
NaN
String(123)
"123"
NaN === NaN
false
if(isNaN(NaN)) {
  console.log("nan")
}
a = 42
42
a.toString()
"42"
String(a)
"42"
