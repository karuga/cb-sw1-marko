let students = ["Aziz", "Amrei", "Felix", "Christian", "Marc", "Astrid", "Dalin"];
undefined
let numbers = [13, 5, 7, -1, 0];
undefined
students
Array(7) [ "Aziz", "Amrei", "Felix", "Christian", "Marc", "Astrid", "Dalin" ]

students[0]
"Aziz"
students[1]
"Amrei"
students[6]
"Dalin"
students.length
7
for (let i=0; i<7; i++) {
  console.log(students[i]);
}

for (let i=0; i<7; i++) {
  console.log("Hello " + students[i]);
}

for (let i=0; i<7; i++) {
  console.log("Hello " + students[i]) + "!";
}

for (let i=0; i<7; i++) {
  console.log("Hello " + students[i] + "!");
}

for (let i=0; i<7; i++) {
  console.log(`Hello ${students[i]}! Your name is ${students[i].length} characters long.`);
}

for (let i=0; i<students.length; i++) {
  let student = students[i];
  if (student.length >= 6) {
    console.log(`Hello ${student}!`);
  }
}

students
// Array(7) [ "Aziz", "Amrei", "Felix", "Christian", "Marc", "Astrid", "Dalin" ]

students[2] = null;

students
// Array(7) [ "Aziz", "Amrei", null, "Christian", "Marc", "Astrid", "Dalin" ]

students.push("Marko");
8
students
// Array(8) [ "Aziz", "Amrei", null, "Christian", "Marc", "Astrid", "Dalin", "Marko" ]

students.pop()
"Marko"
students
// Array(7) [ "Aziz", "Amrei", null, "Christian", "Marc", "Astrid", "Dalin" ]

students.toSorted()
// Array(7) [ "Amrei", "Astrid", "Aziz", "Christian", "Dalin", "Marc", null ]
