console.log("hello");

let myDiv = document.getElementById("my-div");

if (myDiv !== null) {
    myDiv.innerText = "hello ".repeat(20);
}

let yourDiv = document.querySelector("#your-div");

yourDiv.innerText = "hi ".repeat(20);


// all divs with a foo class
let allFooDivs = document.querySelectorAll(".foo");

for (let fooDiv of allFooDivs) {
    fooDiv.innerText += "!!!!!";
}

function showRandomNumber() {
    let randomNumber = Math.ceil(Math.random() * 10);
    let randOutputDiv = document.querySelector("#rand-output");
    randOutputDiv.innerText = String(randomNumber);
}

let randBtn = document.querySelector("#rand-btn");
randBtn.addEventListener("click", showRandomNumber);

let randBtn2 = document.querySelector("#rand-btn-2");
randBtn2.addEventListener("mouseenter", showRandomNumber);

randBtn2.addEventListener("click", () => {
    alert("xxx");
});
