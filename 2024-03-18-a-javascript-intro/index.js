console.log("Hello world from index.js");

let firstName = "Marko";
const lastName = "Knöbl";

firstName = 42;

// ungültig
// lastName = 10;

console.log(firstName);

// Vorschau auf das Manipulieren von HTML-Inhalten
document.body.innerHTML = "<h1>new content</h1>";

console.log(3**2);

console.log(2 == "2");
console.log(2 === "2");

// "truthy" und "falsy"

if (1) {
    console.log("1 ist truthy");
}

if (0) {
    // wird nicht ausgeführt
    console.log("0 ist truthy")
}

if ("") {
    // wird nicht ausgeführt
    console.log("Ein leerer string ist truthy");
}

if (null || undefined) {
    // wird nicht ausgeführt
    console.log("null bzw undefined sind truthy");
}

// Konvertieren in Booleans

console.log(Boolean(0))

function greetPerson(name) {
    console.log("Hello, " + name + "!!");
}

greetPerson("Adam");


// "Pfeilfunktion" / anonyme Funktion

let greetPerson2 = (name) => {
    console.log("Hello, " + name + "!!");
};

greetPerson2("Eve");
