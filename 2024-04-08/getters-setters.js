let person = {
  firstName: "Marko",
  lastName: "Knöbl",

  // TODO: fullName
  get fullName() {
    return this.firstName + " " + this.lastName;
  },
  set fullName(value) {
    // z.B. value = "Peter Parker"  -> nameParts = ["Peter", "Parker"]
    let nameParts = value.split(" ");
    this.firstName = nameParts[0];
    this.lastName = nameParts[1];
  },

  birthYear: 1988,
  get age() {
    return 2024 - this.birthYear;
  },
  set age(newAge) {
    this.birthYear = 2024 - newAge;
  },

  introduceSelf() {
    console.log("Hello, my name is " + this.firstName);
  }
};

// folgendes soll funktionieren:

// getter
console.log(person.fullName);

// setter (soll firstName und lastName ändern)
person.fullName = "Peter Parker";

console.log(person.firstName); // Peter
console.log(person.lastName);  // Parker

person.introduceSelf();


for (let key in person) {
    console.log(key);
}

let arr = [3, 2, 4]

for (let index in arr) {
    console.log(arr[index]);
}
