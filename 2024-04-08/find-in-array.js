const persons = [
    {name: "Alice", birthYear: 1980},
    {name: "Bob", birthYear: 1990},
    {name: "Charlie", birthYear: 2000}
]

// finde alle, die vor 2000 geboren wurden

const result = persons.filter(
    (person) => {
        if (person.birthYear < 2000) {
            return true;
        } else {
            return false;
        }
    }
);

console.log(result);

const firstOldPerson = persons.find(
    (person) => person.birthYear < 2000
)

console.log(firstOldPerson);
